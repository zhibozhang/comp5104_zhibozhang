package testcase.network;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import server.logic.handler.InputHandler;
import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;

public class HandlerTest {
	InputHandler handler;
	@Before
    public void setUp() {
		handler = new InputHandler();
	}
	 @Test
		public void test1() {
		 //case:Determine the identity of clients.
		 assertEquals("Who Are you?Clerk or User?",handler.processInput("Hello",InputHandler.WAITING).getOutput());
		 
		 String msg="Please Input The Password:";
		 assertEquals(msg, handler.processInput("clerk",InputHandler.FINISHWAITING).getOutput());
		 msg="What can I do for you?Menu:Create User/Title/Item,Delete User/Title/Item.";
		 assertEquals(msg, handler.processInput("admin",InputHandler.CLERKLOGIN).getOutput());
		 msg="Wrong Password!Please Input The Password:";
		 assertEquals(msg, handler.processInput("aaa",InputHandler.CLERKLOGIN).getOutput());
		 
		 msg="Please Input Username and Password:'username,password'";
		 assertEquals(msg, handler.processInput("user",InputHandler.FINISHWAITING).getOutput());
		 msg="What can I do for you?Menu:Borrow,Renew,Return,Pay Fine.";
		 assertEquals(msg, handler.processInput("zhibo@carleton.ca,zhibo",InputHandler.USERLOGIN).getOutput());
		 msg="Wrong Password!Please Input Username and Password:'username,password'";
		 assertEquals(msg, handler.processInput("zhibo@carleton.ca,yu",InputHandler.USERLOGIN).getOutput());
		 msg="The User Does Not Exist!Please The Username and Password:'username,password'";
		 assertEquals(msg, handler.processInput("zoe@carleton.ca,yu",InputHandler.USERLOGIN).getOutput());
		 msg="Your input should in this format:'username,password'";
		 assertEquals(msg, handler.processInput("zhibo",InputHandler.USERLOGIN).getOutput());
		}
	 @Test
		public void test2() {
		 //case:the client is clerk.
		 //create user
		 String msg="";
		 msg="Please Input User Info:'username,password'";
		 assertEquals(msg, handler.processInput("create user",InputHandler.CLERK).getOutput());
		 //create title
		 msg="Please Input Title Info:'ISBN,title'";
		 assertEquals(msg, handler.processInput("create title",InputHandler.CLERK).getOutput());
		 //create item
		 msg="Please Input Item Info:'ISBN'";
		 assertEquals(msg, handler.processInput("create item",InputHandler.CLERK).getOutput());
		 //delete user
		 msg="Please Input User Info:'useremail'";
		 assertEquals(msg, handler.processInput("delete user",InputHandler.CLERK).getOutput());
		 //delete title
		 msg="Please Input Title Info:'ISBN'";
		 assertEquals(msg, handler.processInput("delete title",InputHandler.CLERK).getOutput());
		 //delete item
		 msg="Please Input Item Info:'ISBN,copynumber'";
		 assertEquals(msg, handler.processInput("delete item",InputHandler.CLERK).getOutput());
		}
	 
	 @Test
		public void test3() {
		 //case:the client is user.
		 //borrowing
		 String msg="";
		 msg="Please Input User Info:'useremail,ISBN,copynumber'";
		 assertEquals(msg, handler.processInput("borrow",InputHandler.USER).getOutput());
		 //renewing
		 msg="Please Input Title Info:'useremail,ISBN,copynumber'";
		 assertEquals(msg, handler.processInput("renew",InputHandler.USER).getOutput());
		 //returning
		 msg="Please Input Item Info:'useremail,ISBN,copynumber'";
		 assertEquals(msg, handler.processInput("return",InputHandler.USER).getOutput());
		 //pay fine
		 msg="Please Input User Info:'useremail'";
		 assertEquals(msg, handler.processInput("pay fine",InputHandler.USER).getOutput());
		}
	 @Test
		public void test4() {
		 //case:Asynchronous creation and deletion
		 //create user
		 String msg="";
		 msg="Success!";
		 assertEquals(msg, handler.processInput("Tom@carleton.ca,Tom",InputHandler.CREATEUSER).getOutput());
		 msg="The User Already Exists!";
		 assertEquals(msg, handler.processInput("Zhibo@carleton.ca,Zhibo",InputHandler.CREATEUSER).getOutput());
		 msg="Your input should in this format:'username,password'";
		 assertEquals(msg, handler.processInput("Zhibo@carleton.ca",InputHandler.CREATEUSER).getOutput());
		 
		 //create title
		 msg="Success!";
		 assertEquals(msg, handler.processInput("9781137439666,The intoxication of power",InputHandler.CREATETITLE).getOutput());
		 msg="The Title Already Exists!";
		 assertEquals(msg, handler.processInput("9781442668584,The intoxication of power",InputHandler.CREATETITLE).getOutput());
		 msg="Your input should in this format:'ISBN,title',ISBN should be a 13-digit number";
		 assertEquals(msg, handler.processInput("9781442668584:The intoxication of power",InputHandler.CREATETITLE).getOutput());
		 
		 //create item
		 msg="Success!";
		 assertEquals(msg, handler.processInput("9781317594277",InputHandler.CREATEITEM).getOutput());
		 msg="The Title Does Not Exists!";
		 assertEquals(msg, handler.processInput("9781137439777",InputHandler.CREATEITEM).getOutput());
		 msg="Your input should in this format:'ISBN',ISBN should be a 13-digit number";
		 assertEquals(msg, handler.processInput("9781442668584:The intoxication of power",InputHandler.CREATEITEM).getOutput());
		 
		 //delete user
		 msg="Success!";
		 assertEquals(msg, handler.processInput("Kevin@carleton.ca",InputHandler.DELETEUSER).getOutput());
		 msg="The User Does Not Exist!";
		 assertEquals(msg, handler.processInput("LAla@carleton.ca",InputHandler.DELETEUSER).getOutput());
		 msg="Outstanding Fee Exists!";
		 assertEquals(msg, handler.processInput("Zhibo@carleton.ca",InputHandler.DELETEUSER).getOutput());
		 msg="Active Loan Exists!";
		 assertEquals("success",LoanTable.getInstance().createloan(1,"9781611687910","1",new Date(new Date().getTime()-6 * 60 * 1000)));
		 assertEquals(msg, handler.processInput("Yu@carleton.ca",InputHandler.DELETEUSER).getOutput());
		 msg="Your input should in this format:'useremail'";
		 assertEquals(msg, handler.processInput("Zhibo",InputHandler.DELETEUSER).getOutput());
		 
		 //delete title
		 msg="Success!";
		 assertEquals(msg, handler.processInput("9781317594277",InputHandler.DELETETITLE).getOutput());
		 msg="The Title Does Not Exist!";
		 assertEquals(msg, handler.processInput("9781318888888",InputHandler.DELETETITLE).getOutput());
		 msg="Active Loan Exists!";
		 assertEquals(msg, handler.processInput("9781442668584",InputHandler.DELETETITLE).getOutput());
		 msg="Your input should in this format:'ISBN',ISBN should be a 13-digit number";
		 assertEquals(msg, handler.processInput("Zhibo",InputHandler.DELETETITLE).getOutput());
		 
		 //delete item
		 msg="Success!";
		 assertEquals(msg, handler.processInput("9781442667181,1",InputHandler.DELETEITEM).getOutput());
		 msg="The Item Does Not Exist!";
		 assertEquals(msg, handler.processInput("9781442667181,4",InputHandler.DELETEITEM).getOutput());
		 msg="Active Loan Exists!";
		 assertEquals(msg, handler.processInput("9781442668584,1",InputHandler.DELETEITEM).getOutput());
		 msg="Your input should in this format:'ISBN,copynumber',ISBN should be a 13-digit number";
		 assertEquals(msg, handler.processInput("Zhibo",InputHandler.DELETEITEM).getOutput());
		}
	 
	 @Test
		public void test5() {
		 //case:Asynchronous borrowing,renewing,returning and pay fine.
		 String msg="";
		 //borrowing
		 msg="Success!";
		 assertEquals(msg, handler.processInput("Yu@carleton.ca,9781442616899,1",InputHandler.BORROW).getOutput());
		 msg="The User Does Not Exist!";
		 assertEquals(msg, handler.processInput("Zoe@carleton.ca,9781442616899,1",InputHandler.BORROW).getOutput());
		 msg="ISBN Invalid!";
		 assertEquals(msg, handler.processInput("Yu@carleton.ca,9781442616777,1",InputHandler.BORROW).getOutput());
		 msg="Copynumber Invalid!";
		 assertEquals(msg, handler.processInput("Yu@carleton.ca,9781442616899,5",InputHandler.BORROW).getOutput());
		 msg="The Item is Not Available!";
		 assertEquals(msg, handler.processInput("Yu@carleton.ca,9781442616899,1",InputHandler.BORROW).getOutput());
		 msg="The Maximun Number of Items is Reached!";
		 for(int i=0;i<5;i++){
				ItemTable.getInstance().createitem("9781442616899");
			}
			LoanTable.getInstance().createloan(1,"9781442616899","2",new Date());
			LoanTable.getInstance().createloan(1,"9781442616899","3",new Date());
			LoanTable.getInstance().createloan(1,"9781442616899","4",new Date());
			LoanTable.getInstance().createloan(1,"9781442616899","5",new Date());
		 assertEquals(msg, handler.processInput("Yu@carleton.ca,9781442616899,6",InputHandler.BORROW).getOutput());
		 msg="Outstanding Fee Exists!";
		 assertEquals(msg, handler.processInput("Zhibo@carleton.ca,9781442616899,6",InputHandler.BORROW).getOutput());
		
		 
		 //renewing
		 msg="Success!";
		 assertEquals(true,TitleTable.getInstance().createtitle("1234567891234", "Turing Game"));
		 assertEquals(true,ItemTable.getInstance().createitem("1234567891234"));
		 assertEquals("success",LoanTable.getInstance().createloan(2, "1234567891234", "1", new Date(new Date().getTime()-2 * 60 * 1000)));
		 assertEquals(msg, handler.processInput("Michelle@carleton.ca,1234567891234,1",InputHandler.RENEW).getOutput());
		 msg="The User Does Not Exist!";
		 assertEquals(msg, handler.processInput("Zoe@carleton.ca,9781442667181,1",InputHandler.RENEW).getOutput());
		 msg="The Maximun Number of Items is Reached!";
		 assertEquals(msg, handler.processInput("Yu@carleton.ca,9781442616899,6",InputHandler.RENEW).getOutput());
		 msg="Outstanding Fee Exists!";
		 assertEquals(msg, handler.processInput("Zhibo@carleton.ca,9781442668584,1",InputHandler.RENEW).getOutput());
		 msg="Renewed Item More Than Once for the Same Loan!";
		 assertEquals(msg, handler.processInput("Michelle@carleton.ca,1234567891234,1",InputHandler.RENEW).getOutput());
		 
		 //returning
		 msg="Success!";
		 assertEquals(msg, handler.processInput("Zhibo@carleton.ca,9781442668584,1",InputHandler.RETURN).getOutput());
		 msg="The Loan Does Not Exist!";
		 assertEquals(msg, handler.processInput("Yu@carleton.ca,9781442668584,1",InputHandler.RETURN).getOutput());
		 
		 //pay fine
		 msg="Borrowing Items Exist!";
		 assertEquals(msg, handler.processInput("Michelle@carleton.ca",InputHandler.PAYFINE).getOutput());
		 msg="Success!";
		 assertEquals("success",FeeTable.getInstance().payfine(0));
		 assertEquals(msg, handler.processInput("Zhibo@carleton.ca",InputHandler.PAYFINE).getOutput());
		}
	@Test
	 public void test6() {
		 //case:Options for user to return to main menu and log out.
		 //main menu
		 String msg="";
		 msg="What can I do for you?Menu:Create User/Title/Item,Delete User/Title/Item.";
		 assertEquals(msg, handler.processInput("main menu",InputHandler.CLERK).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.CREATEUSER).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.CREATETITLE).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.CREATEITEM).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.DELETEUSER).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.DELETETITLE).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.DELETEITEM).getOutput());
		 
		 msg="What can I do for you?Menu:Borrow,Renew,Return,Pay Fine.";
		 assertEquals(msg, handler.processInput("main menu",InputHandler.USER).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.BORROW).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.RENEW).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.RETURN).getOutput());
		 assertEquals(msg, handler.processInput("main menu",InputHandler.PAYFINE).getOutput());
		 
		 //logout
		 msg="Successfully Log Out!";
		 assertEquals(msg, handler.processInput("log out",InputHandler.CLERK).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.CREATEUSER).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.CREATETITLE).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.CREATEITEM).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.DELETEUSER).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.DELETETITLE).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.DELETEITEM).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.USER).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.BORROW).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.RENEW).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.RETURN).getOutput());
		 assertEquals(msg, handler.processInput("log out",InputHandler.PAYFINE).getOutput());
		}
}
