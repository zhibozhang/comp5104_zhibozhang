package testcase.functionality;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class RenewalEngine {
	@BeforeClass
	//set up the initial table
	public static void initialTabels(){
		UserTable.getInstance();
		TitleTable.getInstance();
		ItemTable.getInstance();
		LoanTable.getInstance();
		//initial a new loan to test for case in test(),stimulated 2 day(minutes) early
		assertEquals("success",LoanTable.getInstance().createloan(2, "9781442667181", "1", new Date(new Date().getTime()-2 * 60 * 1000)));
	}

	@Test
	public void test() {
		assertEquals("success",LoanTable.getInstance().renewal(2,"9781442667181","1",new Date()));
	}
	
	@Test
	public void test1() {
		//case:user try to renew invalid loan
		assertEquals("The loan does not exist",LoanTable.getInstance().renewal(6,"9781442667181","1",new Date()));
	}
	
	@Test
	public void test2() {
		//case:The user is not allowed to renew any items,since he has already borrowed 3 items.
		for(int i=0;i<3;i++){
			assertEquals(true,ItemTable.getInstance().createitem("9781442616899"));
		}
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442616899","2",new Date()));
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442616899","3",new Date()));
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442616899","4",new Date()));
		assertEquals("The Maximun Number of Items is Reached",LoanTable.getInstance().renewal(1,"9781442616899","4",new Date()));
	}
	
	@Test
	public void test3() {
		//Case:The user is not allowed to renew any items,since he has outstanding fee.
		assertEquals("Outstanding Fee Exists",LoanTable.getInstance().renewal(0,"9781442668584","1",new Date()));
	}
	
	@Test
	public void test4() {
		//Case:The user can not renewed for the specific book more than once for this loan.
		assertEquals("Renewed Item More Than Once for the Same Loan",LoanTable.getInstance().renewal(2,"9781442667181","1",new Date()));
	}
	

}
