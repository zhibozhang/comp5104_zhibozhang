package testcase.functionality;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.UserTable;

public class UserCreation {

	
	@BeforeClass
	//set up the initial table
	public static void initialUser(){
		UserTable.getInstance();
	}

	@Test
	//case 1 :Successfully create user test
	public void test() {
		assertEquals(true,UserTable.getInstance().createuser("Tom@carleton.ca","Tom"));
		}
	
	@Test
	//case 2 :Fail to create an existed user
	public void test1() {
		assertEquals(false,UserTable.getInstance().createuser("Zhibo@carleton.ca","Zhibo"));
		}
}
