package testcase.functionality;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class FineEngine {
	
	@BeforeClass
	//set up the initial table
	public static void initialTabels(){
		UserTable.getInstance();
		TitleTable.getInstance();
		ItemTable.getInstance();
		LoanTable.getInstance();
	}

	@Test
	public void test() {
		//Every time get an instance of fee,fee will be recalculated
		for(int i=0;i<6;i++){
			assertEquals(true,ItemTable.getInstance().createitem("9781442616899"));
		}
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442616899","3",new Date()));
		assertEquals("success",LoanTable.getInstance().createloan(2,"9781442616899","4",new Date(new Date().getTime()-6 * 60 * 1000)));
		assertEquals("success",LoanTable.getInstance().createloan(3,"9781442616899","5",new Date(new Date().getTime()-3 * 60 * 1000)));
		assertEquals("success",LoanTable.getInstance().createloan(4,"9781442616899","6",new Date(new Date().getTime()-5 * 60 * 1000)));
		FeeTable.getInstance().Initialization();
		assertEquals(5,FeeTable.getInstance().lookupfee(0));
		assertEquals(0,FeeTable.getInstance().lookupfee(1));
		assertEquals(1,FeeTable.getInstance().lookupfee(2));
		assertEquals(0,FeeTable.getInstance().lookupfee(3));
		assertEquals(0,FeeTable.getInstance().lookupfee(4));
	}
	@Test
	public void test1() {
		//Pay Fine.Case:The user still involving borrowing.
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781611687910","1",new Date()));
		assertEquals("Borrowing Items Exist",FeeTable.getInstance().payfine(0));
	}
	@Test
	public void test2() {
		//Pay Fine.Case:The user still involving renewing.
		assertEquals("success",LoanTable.getInstance().renewal(1,"9781442616899","3",new Date()));
		assertEquals("Borrowing Items Exist",FeeTable.getInstance().payfine(0));
	}
	@Test
	public void test3() {
		//Pay Fine.
		assertEquals("success",LoanTable.getInstance().returnItem(0,"9781442668584","1",new Date()));
		assertEquals("success",FeeTable.getInstance().payfine(0));
		assertEquals(0,FeeTable.getInstance().lookupfee(0));
	}

}
