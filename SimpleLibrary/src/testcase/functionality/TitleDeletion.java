package testcase.functionality;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class TitleDeletion {
	@BeforeClass
	//set up the initial table
	public static void initialTabels(){
		UserTable.getInstance();
		TitleTable.getInstance();
		ItemTable.getInstance();
		LoanTable.getInstance();
		FeeTable.getInstance();
	}
	@Test
	public void test() {
		assertEquals("success",TitleTable.getInstance().delete("9781317594277"));
	}
	@Test
	public void test1() {
		//case:title does not exist
		assertEquals("The Title Does Not Exist",TitleTable.getInstance().delete("9781318888888"));
	}
	@Test
	public void test2() {
		//case:Some copies of the title are on loan.
		assertEquals("Active Loan Exists",TitleTable.getInstance().delete("9781442668584"));
	}

}
