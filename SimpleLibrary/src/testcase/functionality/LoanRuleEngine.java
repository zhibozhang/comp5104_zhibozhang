package testcase.functionality;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class LoanRuleEngine {
	@BeforeClass
	//set up the initial table
	public static void initialTabels(){
		UserTable.getInstance();
		TitleTable.getInstance();
		ItemTable.getInstance();
		LoanTable.getInstance();
	}

	@Test
	public void test() {
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442616899","1",new Date()));
	}
	
	@Test
	public void test1() {
		//Case 1:The user is invalid
		assertEquals("User Invalid",LoanTable.getInstance().createloan(6,"9781442616899","1",new Date()));
		//Case 2:The ISBN is invalid
		assertEquals("ISBN Invalid",LoanTable.getInstance().createloan(1,"9781442616777","1",new Date()));
		//Case 3:The copynumber is invalid
		assertEquals("Copynumber Invalid",LoanTable.getInstance().createloan(1,"9781442616899","5",new Date()));
	}
	
	@Test
	public void test2() {
		//Fail to add same loan twice(same information compared to test())
		//The item is already borrowed,not available
		assertEquals("The Item is Not Available",LoanTable.getInstance().createloan(1,"9781442616899","1",new Date()));
	}
	
	@Test
	public void test3() {
		//Case:The user is not allowed to borrow any items,since he has already borrowed 3 items.
		for(int i=0;i<5;i++){
			assertEquals(true,ItemTable.getInstance().createitem("9781442616899"));
		}
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442616899","2",new Date()));
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442616899","3",new Date()));
		assertEquals("The Maximun Number of Items is Reached",LoanTable.getInstance().createloan(1,"9781442616899","4",new Date()));
	}
	
	@Test
	public void test4() {
		//Case:The user is not allowed to borrow any items,since he has outstanding fee.
		assertEquals("Outstanding Fee Exists",LoanTable.getInstance().createloan(0,"9781442616899","6",new Date()));
	}

}
