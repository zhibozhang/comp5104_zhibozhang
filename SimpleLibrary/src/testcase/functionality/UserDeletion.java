package testcase.functionality;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class UserDeletion {
	@BeforeClass
	//set up the initial table
	public static void initialTabels(){
		UserTable.getInstance();
		TitleTable.getInstance();
		ItemTable.getInstance();
		LoanTable.getInstance();
		FeeTable.getInstance();
	}
	
	@Test
	public void test() {
		assertEquals("success",UserTable.getInstance().delete(4));
	}
	@Test
	public void test1() {
		//case:The User Does Not Exist.
		assertEquals("The User Does Not Exist",UserTable.getInstance().delete(5));
	}
	@Test
	public void test2() {
		//case:Outstanding Fee Exists.
		assertEquals("Outstanding Fee Exists",UserTable.getInstance().delete(0));
	}
	@Test
	public void test3() {
		//case:Active Loan Exists
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781611687910","1",new Date(new Date().getTime()-6 * 60 * 1000)));
		assertEquals("Active Loan Exists",UserTable.getInstance().delete(1));
	}

}
