package testcase.functionality;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.TitleTable;

public class TitleCreation {

	@BeforeClass
	//set up the initial table
	public static void initialTitle(){
		TitleTable.getInstance();
	}
	
	@Test
	//case 1 :Successfully create new title
	public void test() {
		assertEquals(true,TitleTable.getInstance().createtitle("9781137439666","The intoxication of power"));
		}
	
	@Test
	//case 2 :fail to create new title with existed ISBN
	public void test1() {
		assertEquals(false,TitleTable.getInstance().createtitle("9781442668584","The intoxication of power"));
		}

}
