package testcase.functionality;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class ItemDeletion {
	@BeforeClass
	//set up the initial table
	public static void initialTabels(){
		UserTable.getInstance();
		TitleTable.getInstance();
		ItemTable.getInstance();
		LoanTable.getInstance();
		FeeTable.getInstance();
	}

	@Test
	public void test() {
		assertEquals("success",ItemTable.getInstance().delete("9781442667181","1"));
	}
	
	@Test
	public void test1() {
		//case:the item does not exist.
		assertEquals("The Item Does Not Exist",ItemTable.getInstance().delete("9781442667181","4"));
	}
	@Test
	public void test2() {
		//case:The item is currently on loan.
		assertEquals("Active Loan Exists",ItemTable.getInstance().delete("9781442668584","1"));
	}


}
