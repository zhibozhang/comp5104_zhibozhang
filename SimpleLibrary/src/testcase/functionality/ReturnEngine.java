package testcase.functionality;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.FeeTable;
import server.logic.tables.ItemTable;
import server.logic.tables.LoanTable;
import server.logic.tables.TitleTable;
import server.logic.tables.UserTable;

public class ReturnEngine {
	@BeforeClass
	//set up the initial table
	public static void initialTabels(){
		UserTable.getInstance();
		TitleTable.getInstance();
		ItemTable.getInstance();
		LoanTable.getInstance();
	}

	@Test
	public void test() {
		assertEquals("success",LoanTable.getInstance().returnItem(0,"9781442668584","1",new Date()));
	}
	
	@Test
	public void test1() {
		//case:the loan is invalid
		assertEquals("The Loan Does Not Exist",LoanTable.getInstance().returnItem(1,"9781442668584","1",new Date()));
	}
	
	@Test
	public void test2() {
		//case:after the 6th stimulated day(6 minutes),fee needs to be applied
		assertEquals("success",LoanTable.getInstance().createloan(1,"9781442668584","1",new Date(new Date().getTime()-6 * 60 * 1000)));
		assertEquals("success",LoanTable.getInstance().returnItem(1,"9781442668584","1",new Date()));
		assertEquals(1,FeeTable.getInstance().lookupfee(1));
	}

}
