package testcase.functionality;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import server.logic.tables.ItemTable;

public class ItemCreation {

	@BeforeClass
	//set up the initial table
	public static void initialTitle(){
		ItemTable.getInstance();
	}
	
	@Test
	public void test() {
		//case 1:create the first item of such ISBN
		assertEquals(true,ItemTable.getInstance().createitem("9781317594277"));
		//case 1:create not the first item of such ISBN,to check the copy number whether unique or not
		assertEquals(true,ItemTable.getInstance().createitem("9781442668584"));
		}
	
	@Test
	//Fail to create an item of none-existed ISBN.
	public void test1() {
		assertEquals(false,ItemTable.getInstance().createitem("9781137439777"));
		}

}
